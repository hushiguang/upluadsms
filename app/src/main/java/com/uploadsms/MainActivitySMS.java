package com.uploadsms;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivitySMS extends AppCompatActivity {

    private SubscriptionManager mSubscriptionManager;

    // 是否是双卡
    public boolean isMultiSimEnabled = false;

    // 当前所有的电话数据
    public List<SubscriptionInfo> subInfoList = new ArrayList<>();
    public ArrayList<SimInfo> phoneNumbers = new ArrayList<>();
    // 当前用户手机号
    public String activeNumber;

    PermissionPageUtils permissionPageUtils;
    public final Uri SMS_MESSAGE_URI = Uri.parse("content://sms");
    private SmsDatabaseChaneObserver mSmsDBChangeObserver = null;

    EditText regularEdit;
    Button openPermissionBtn;
    TextView resultText;
    TextView httpText;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = this;
        permissionPageUtils = new PermissionPageUtils(this);
        mSubscriptionManager = SubscriptionManager.from(context);

        regularEdit = findViewById(R.id.regular_edit);
        openPermissionBtn = findViewById(R.id.open_permission);
        resultText = findViewById(R.id.resultText);
        httpText = findViewById(R.id.httpText);
        openPermissionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionPageUtils.jumpPermissionPage();
            }
        });
        logic();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @PermissionSuccess(requestCode = 100)
    public void doRegisterReceiver() {
        GetCarriorsInformation();
        registerSmsDatabaseChangeObserver(this);
    }

    @PermissionFail(requestCode = 100)
    public void doFailRegisterReceiver() {
        Toast.makeText(this, "SMS permission is not granted", Toast.LENGTH_SHORT).show();
        // 跳转权限
        permissionPageUtils.jumpPermissionPage();
    }

    void logic() {
        //授权
        PermissionGen.with(MainActivitySMS.this)
                .addRequestCode(100)
                .permissions(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS)
                .request();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterSmsDatabaseChangeObserver(this);
    }

    @SuppressLint("HardwareIds")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void GetCarriorsInformation() {
        phoneNumbers.clear();
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            permissionPageUtils.jumpPermissionPage();
            return;
        }

        subInfoList = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subInfoList.size() > 1) {
            isMultiSimEnabled = true;
        }
        for (SubscriptionInfo subscriptionInfo : subInfoList) {
            phoneNumbers.add(new SimInfo(subscriptionInfo.getNumber(), subscriptionInfo.getSubscriptionId()));
        }
        Log.d("TAG", phoneNumbers.toString());
    }


    private void registerSmsDatabaseChangeObserver(ContextWrapper contextWrapper) {
        //因为，某些机型修改rom导致没有getContentResolver
        try {
            mSmsDBChangeObserver = new SmsDatabaseChaneObserver(contextWrapper.getContentResolver(), new Handler(),
                    new SmsDatabaseChaneObserver.MessageListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onReceived(SMSModel phoneMessage) {
                            if (!FilterUtil.filer(phoneMessage.getBody(),
                                    regularEdit.getText().toString())) return;
                            if (getResultActiveNumber(phoneMessage)) return;

                            resultText.setText("发信人 :" + phoneMessage.getSender()
                                    + " \n收信人 " + activeNumber
                                    + " \n内容 " + phoneMessage.getBody()
                                    + " \n时间戳 " + phoneMessage.getDate());
                            // 上传
                            uploadMsg(phoneMessage);

                        }
                    });
            contextWrapper.getContentResolver().registerContentObserver(SMS_MESSAGE_URI, true, mSmsDBChangeObserver);
        } catch (Throwable b) {
        }
    }

    private void unregisterSmsDatabaseChangeObserver(ContextWrapper contextWrapper) {
        try {
            contextWrapper.getContentResolver().unregisterContentObserver(mSmsDBChangeObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取接收人的手机号
     *
     * @param phoneMessage
     * @return
     */
    private boolean getResultActiveNumber(SMSModel phoneMessage) {
        if (phoneNumbers.isEmpty()) {
            return true;
        }
        if (phoneMessage.getSimId().equals(phoneNumbers.get(0).getSimId())) {
            activeNumber = phoneNumbers.get(0).getNumber();
        } else {
            if (isMultiSimEnabled) {
                activeNumber = phoneNumbers.get(1).getNumber();
            }
        }
        return false;
    }


    private void uploadMsg(SMSModel phoneMessage) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        FormBody formBody = new FormBody
                .Builder()
                .add("mobile", activeNumber)
                .add("text", phoneMessage.getBody())
                .add("create_at", phoneMessage.getDate().toString())
                .build();
        Request request = new Request
                .Builder()
                .post(formBody)
                .url(ApiConstant.UPLOAD_MSG).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull final IOException e) {
                Log.d("TAG", e.getMessage());
                httpText.post(new Runnable() {
                    @Override
                    public void run() {
                        httpText.setText(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                Log.d("TAG", response.toString());
                httpText.post(new Runnable() {
                    @Override
                    public void run() {
                        httpText.setText(response.toString());
                    }
                });
            }
        });

    }

}