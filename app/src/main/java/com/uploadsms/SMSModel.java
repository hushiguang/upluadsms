package com.uploadsms;

public class SMSModel {

    private String sender;
    private String body;
    private Long date;
    private Integer simId;

    public SMSModel(String sender, String body, Long date, Integer simId) {
        this.sender = sender;
        this.body = body;
        this.date = date;
        this.simId = simId;
    }

    public Integer getSimId() {
        return simId;
    }

    public void setSimId(Integer simId) {
        this.simId = simId;
    }

    @Override
    public String toString() {
        return "SMSModel{" +
                "sender='" + sender + '\'' +
                ", body='" + body + '\'' +
                ", date=" + date +
                ", simId=" + simId +
                '}';
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
