package com.uploadsms;

public class SimInfo {
    String number;
    Integer simId;


    public SimInfo(String number, Integer simId) {
        this.number = number;
        this.simId = simId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getSimId() {
        return simId;
    }

    public void setSimId(Integer simId) {
        this.simId = simId;
    }

    @Override
    public String toString() {
        return "SimInfo{" +
                "number='" + number + '\'' +
                ", simId=" + simId +
                '}';
    }
}
